package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> 
{
	Integer getSize();
	void add(T agregar);
	void addAtEnd(T agregar);
	void addAtK(T agregar, int posicion);
	T getElement(int posicion);
	T getCurrentElement();
	void delete(T eliminar);
	void deleteAtK( int posicion);
	T next();
	T previous();
}
