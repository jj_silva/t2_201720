package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import api.ISTSManager;
import model.vo.VORoute;
import model.vo.VOStop;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.RingList;

public class STSManager<T> implements ISTSManager {


	public void loadRoutes(String routesFile)
	{
		String csvFile = routesFile;
		BufferedReader br = null;
		String line = "";
		int n = 0;
		String cvsSplitBy = "\n";
		try
		{
			DoubleLinkedList<Object> lista = new DoubleLinkedList<Object>();
			br = new BufferedReader(new FileReader(csvFile));
			String[] datos = null;
			while ((line = br.readLine()) != null)
			{

			}

		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally 
		{
			if (br != null)
			{
				try
				{
					br.close();
				}
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}



	public void loadTrips(String tripsFile) 
	{
		String csvFile = tripsFile;
		BufferedReader br = null;
		String line = "";
		try
		{
			RingList<T> lista = new RingList<T>();
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null)
			{
				String[] datos = line.split(",");
				if(!(datos[0].equals("route_id")))
				{
					lista.add( (T) line);
				}	
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally 
		{
			if (br != null)
			{
				try
				{
					br.close();
				}
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}

	}


	public void loadStops(String stopsFile) 
	{
		String csvFile = stopsFile;
		BufferedReader br = null;
		String line = "";
		try
		{
			RingList<T> lista  = new RingList<T>();
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null)
			{
				String[] datos = line.split(",");
				if(!(datos[0].equals("stop_id")))
				{
					lista.add((T) line);
				}	
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally 
		{
			if (br != null)
			{
				try
				{
					br.close();
				}
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}

	}


	public IList<VORoute> routeAtStop(String stopName) 
	{

		return null;
	}


	public IList<VOStop> stopsRoute(String routeName, String direction) 
	{

		return null;
	}

	public void loadStopTimes(String stopTimesFile)
	{
		String csvFile = stopTimesFile;
		BufferedReader br = null;
		String line = "";
		try
		{
			DoubleLinkedList<Object> lista = new DoubleLinkedList<Object>();
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null)
			{
				String[] datos = line.split(",");
				if(!(datos[0].equals("trip_id")))
				{
					lista.add(line);
				}	
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally 
		{
			if (br != null)
			{
				try
				{
					br.close();
				}
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}

}
